# Phiscal
The Business Information System

## Description
Repo defines a VueJS (JavaScript Library) web application, interfacing the Phiscus RESTful API to [Phiscal](https://api.phiscal.site), an online Business Information System, facilitating functionality of different induries, by means of sub-components, dubbed **"Modules"**.
Application is licensed under [the GNU Public License version 3](https://www.gnu.org/licenses/gpl-3.0.txt).

## Dependencies
* Node Package Manager (npm)
* Active Phiscus RESTful API

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
