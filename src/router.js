import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: () => import('@/view/layout/website/Main'),
    children: [
      {
        path: '',
        name: 'home',
        meta: { title: 'Home' },
        component: () => import('@/view/page/website/home/Home')
      },
      {
        path: '/tnc',
        name: 'tnc',
        meta: { title: 'Terms of Service' },
        component: () => import('@/view/page/website/TnC')
      },
      {
        path: '/signedup',
        name: 'signedup',
        meta: { title: 'Email Sent' },
        component: () => import('@/view/page/website/Signedup')
      },
      {
        path: '/redirect/:action/:target/:token',
        name: 'redirect',
        meta: { title: 'Verification' },
        component: () => import('@/view/page/website/Redirect')
      }
    ]
  },
  {
    path: '/auth',
    component: () => import('@/view/layout/Auth'),
    children: [
      {
        path: '',
        name: 'login',
        meta: { title: 'Sign In' },
        component: () => import('@/view/page/auth/Login')
      },
      {
        path: '/register',
        name: 'register',
        meta: { title: 'Sign Up' },
        component: () => import('@/view/page/auth/Register')
      },
      {
        path: '/forgot',
        name: 'forgot',
        meta: { title: 'Reset Password' },
        component: () => import('@/view/page/auth/Forgot')
      },
      {
        path: '/contactus',
        name: 'contactus',
        meta: { title: 'Inquiries' },
        component: () => import('@/view/page/auth/ContactUs')
      }
    ]
  },
  {
    path: '/',
    redirect: '/dash',
    component: () => import('@/view/layout/dash/Main'),
    children: [
      {
        path: '/dash',
        name: 'monitor',
        meta: { title: 'Monitor' },
        component: () => import('@/view/page/dash/Monitor.vue')
      },
      {
        path: '/admin',
        name: 'admin',
        meta: { title: 'Admin' },
        component: () => import('@/view/page/dash/Admin.vue')
      },
      {
        path: '/org',
        component: () => import('@/view/layout/dash/section/Organization.vue'),
        children: [
          {
            path: '',
            name: 'organization',
            meta: { title: 'Business Info' },
            component: () => import('@/view/page/dash/org/Index.vue')
          },
          {
            path: '/org/logo/:id',
            name: 'orglogo',
            meta: { title: 'Business Logo', parent: 'organization' },
            component: () => import('@/view/page/dash/org/Logo.vue')
          },
          {
            path: '/org/update',
            name: 'orgupdate',
            meta: { title: 'Business Update', parent: 'organization' },
            component: () => import('@/view/page/dash/org/Update.vue')
          },
          {
            path: '/org/paygate',
            name: 'paygate',
            meta: { title: 'Payments', parent: 'organization' },
            component: () => import('@/view/page/dash/org/PayGate.vue')
          }
        ]
      },
      {
        path: '/modules',
        name: 'modules',
        meta: { title: 'Module Select' },
        component: () => import('@/view/page/dash/module/List.vue')
      },
      {
        path: '/modules/:id',
        name: 'module',
        meta: { title: 'Module Details', parent: 'modules' },
        component: () => import('@/view/page/dash/module/Details.vue')
      },
      {
        path: '/registration/role',
        name: 'roleselect',
        meta: { title: 'Welcome Note', parent: 'modules' },
        component: () => import('@/view/page/wizard/RoleSelect.vue')
      },
      {
        path: '/registration/structure',
        name: 'structure',
        meta: { title: 'Welcome Note', parent: 'roleselect' },
        component: () => import('@/view/page/wizard/Structure.vue')
      },
      {
        path: '/registration/formal',
        component: () => import('@/view/layout/dash/section/Welcome.vue'),
        children: [
          {
            path: '',
            name: 'regformal',
            meta: { title: 'Welcome Note', parent: 'structure' },
            component: () => import('@/view/page/wizard/welcome/Formal.vue')
          },
          {
            path: '/registration/freelance',
            name: 'regfreelance',
            meta: { title: 'Welcome Note', parent: 'structure' },
            component: () => import('@/view/page/wizard/welcome/Freelance.vue')
          },
          {
            path: '/registration/headquarters',
            name: 'headquarters',
            meta: { section: 'Welcome', title: 'Headquarters', parent: 'regformal' },
            component: () => import('@/view/page/wizard/welcome/Headquarters.vue')
          },
          {
            path: '/registration/homeaddress',
            name: 'homeaddress',
            meta: { section: 'Welcome', title: 'Home Address', parent: 'regfreelance' },
            component: () => import('@/view/page/wizard/welcome/HomeAddress.vue')
          },
          {
            path: '/registration/gateway',
            name: 'paymentgateway',
            meta: { section: 'Welcome', title: 'Payment Gateway!' },
            component: () => import('@/view/page/wizard/welcome/Gateway.vue')
          },
          {
            path: '/registration/complete',
            name: 'registrationcomplete',
            meta: { section: 'Welcome', title: 'Done!', parent: 'paymentgateway' },
            component: () => import('@/view/page/wizard/welcome/Complete.vue')
          }
        ]
      },
      {
        path: '/profile/personal',
        component: () => import('@/view/layout/dash/section/Profile.vue'),
        children: [
          {
            path: '',
            name: 'PersonalDetails',
            meta: { title: 'Personal Details' },
            component: () => import('@/view/page/profile/Personal.vue')
          },
          {
            path: '/profile/address',
            name: 'PhysicalAddress',
            meta: { title: 'Physical Address' },
            component: () => import('@/view/page/profile/Address.vue')
          },
          {
            path: '/profile/account',
            name: 'AccountDetails',
            meta: { title: 'Account Details' },
            component: () => import('@/view/page/profile/Account.vue')
          },
          {
            path: '/profile/password',
            name: 'AccountPassword',
            meta: { title: 'Account Password' },
            component: () => import('@/view/page/profile/Password.vue')
          },
          {
            path: '/profile/business',
            name: 'IndividualBusiness',
            meta: { title: 'Individual Business' },
            component: () => import('@/view/page/profile/Business.vue')
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/404'
  },
  {
    path: '/404',
    component: () => import('@/view/page/error/Error.vue'),
    children: [
      {
        path: '',
        name: '404',
        meta: { title: 'Page Not Found' },
        component: () => import('@/view/page/error/Error-1.vue')
      }
    ]
  }
];

export const router = new VueRouter({
  routes
});
export const { isNavigationFailure, NavigationFailureType } = VueRouter;

export default { router, isNavigationFailure, NavigationFailureType };
