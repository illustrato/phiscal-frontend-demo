'use strict';

/**
 * @class Http class that provides world geography helper functions
 */
var Http = function (resolve, reject) {
  // Main object
  var the = this;

  //////////////////////////
  // * Public Functions * //
  //////////////////////////

  /**
   * Promise.then
   */
  the.followup = function ({ status, headers, data }) {
    if (status >= 200 && status < 300) {
      resolve({ headers: headers, payload: data.value });
    } else {
      throw { status: status, errors: data.errors };
    }
  };

  /**
   * Promise.catch
   */
  the.error = function (error) {
    reject(error);
  };

  return the;
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Http;
}

export default Http;
