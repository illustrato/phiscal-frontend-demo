'use strict';

import { REMOVE_BODY_CLASSNAME } from '@/core/store/template/htmlclass.module';
const touchMap = new WeakMap();
/**
 * @class Crop class provides image cropping helper functions
 */

var Gui = function (cmp) {
  // Main object
  var the = this;

  //////////////////////////
  // * Public Functions * //
  //////////////////////////

  /**
   * Message Reporting
   */
  the.toast = function (messages, title = 'Request Error', variant = 'danger', noAutoHide = true) {
    if (!Array.isArray(messages)) {
      switch (typeof messages) {
        case 'string':
          messages = [messages];
          break;
        case 'object':
          messages = messages.errors || [messages.toString()];
          break;
      }
    }
    cmp.$store.dispatch(REMOVE_BODY_CLASSNAME, 'page-loading');
    for (let message of messages) {
      cmp.$bvToast.toast(message, {
        title: title,
        variant: variant,
        solid: true,
        noAutoHide: noAutoHide
      });
    }
  };

  /**
   * Confirm Box
   */
  the.rmTarget = function (question, title = null) {
    return cmp.$bvModal
      .msgBoxConfirm(question, {
        title: title,
        contentClass: 'rounded-0',
        size: 'sm',
        okVariant: 'danger',
        okTitle: 'YES',
        cancelTitle: 'NO',
        headerClass: 'py-2',
        footerClass: 'p-0 border-top border-top-light-dark'
      })
      .catch((ex) => {
        this.$bvToast.toast(ex, {
          title: 'Message Box Error',
          variant: 'danger',
          solid: true,
          noAutoHide: true
        });
      });
  };

  /**
   * Fancy Message Reporting
   */
  the.statusReport = function (message) {
    if (message === false) {
      return;
    }
    message = typeof message === 'string' ? message : 'Update Succeded';
    // Use a shorter name for this.$createElement
    const h = cmp.$createElement;
    // Create the message
    const vNodesMsg = h('h3', { class: ['text-center', 'mb-0'] }, [
      h('b-spinner', { props: { type: 'grow', small: true } }),
      ` ${message} `,
      h('b-spinner', { props: { type: 'grow', small: true } })
    ]);
    // Create the title
    const vNodesTitle = h('div', { class: ['d-flex', 'flex-grow-1', 'align-items-baseline', 'mr-2'] }, [
      h('strong', { class: 'mr-2' }, 'Status Report'),
      h('small', { class: 'ml-auto text-italics' }, 'Just Now...')
    ]);
    // Pass the VNodes as an array for message and title
    cmp.$bvToast.toast([vNodesMsg], {
      title: [vNodesTitle],
      solid: true,
      variant: 'success',
      toaster: 'b-toaster-top-center'
    });
  };

  /**
   * Delay Control Validation
   */
  the.delayTouch = function ($v, timeout = 3000) {
    $v.$reset();
    if (touchMap.has($v)) {
      clearTimeout(touchMap.get($v));
    }
    touchMap.set($v, setTimeout($v.$touch, timeout));
  };

  /*
   * Validate Control
   */
  the.validateState = function (name) {
    const { $dirty, $error } = cmp.$v.form[name];
    return $dirty ? !$error : null;
  };

  return the;
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Gui;
}

export default Gui;
