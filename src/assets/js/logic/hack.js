export const bool = (val) => {
  if (typeof val === 'boolean') {
    return val;
  }
  let temp = { 1: true, 0: false };
  return val.toString().trim().length === 1 ? temp[val] : val;
};

export default { bool };
