'use strict';

/**
 * @class Geo class that privides world geography helper functions
 */
var Passwd = function (cmp, LENGTH = 8) {
  // Main object
  var the = this;

  ////////////////////////////
  // * Private Functions  * //
  ////////////////////////////

  var Plugin = {
    upperCase: function (txt) {
      for (let i = 0, len = txt.length; i < len; i++) {
        if (txt.charCodeAt(i) >= 65 && txt.charCodeAt(i) <= 90) return true;
      }
      return false;
    },
    lowerCase: function (txt) {
      for (let i = 0, len = txt.length; i < len; i++) {
        if (txt.charCodeAt(i) >= 97 && txt.charCodeAt(i) <= 122) return true;
      }
      return false;
    },
    numeric: function (txt) {
      for (let i = 0, len = txt.length; i < len; i++) {
        if (txt.charCodeAt(i) >= 48 && txt.charCodeAt(i) <= 57) return true;
      }
      return false;
    },
    symbol: function (txt) {
      for (let i = 0, len = txt.length; i < len; i++) {
        if (
          txt.charCodeAt(i) < 48 ||
          (txt.charCodeAt(i) > 57 && txt.charCodeAt(i) < 65) ||
          (txt.charCodeAt(i) > 90 && txt.charCodeAt(i) < 97) ||
          txt.charCodeAt(i) > 122
        )
          return true;
      }
      return false;
    },
    minLength: function (txt) {
      if (txt.length >= LENGTH) return true;
      return false;
    }
  };

  //////////////////////////
  // * Public Functions * //
  //////////////////////////

  /**
   * Reset Formatting
   */
  the.reset = function (id = 'pStrength') {
    for (let i = 1; i < 3; i++) {
      for (let k = 0; k < 5; k++) {
        cmp.$refs[`${i}${id}${k}`].classList = '';
      }
    }
  };

  /**
   * Check Password Strength against Requirements
   */
  the.strength = function (tf, value, id = 'pStrength') {
    const ITEMS = 5;

    for (let i = 0; i < ITEMS; i++) cmp.$refs[`${tf}${id}${i}`].classList = '';
    if (value.length < 1) return false;

    let count = 0;

    if (Plugin.upperCase(value)) {
      cmp.$refs[`${tf}${id}0`].classList.add('bold');
      count++;
    }
    if (Plugin.lowerCase(value)) {
      cmp.$refs[`${tf}${id}1`].classList.add('bold');
      count++;
    }
    if (Plugin.numeric(value)) {
      cmp.$refs[`${tf}${id}2`].classList.add('bold');
      count++;
    }
    if (Plugin.symbol(value)) {
      cmp.$refs[`${tf}${id}3`].classList.add('bold');
      count++;
    }
    if (Plugin.minLength(value)) {
      cmp.$refs[`${tf}${id}4`].classList.add('bold');
      count++;
    }

    return count === ITEMS;
  };

  return the;
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = Passwd;
}

export default Passwd;
