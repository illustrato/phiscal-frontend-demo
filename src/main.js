import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App.vue';
import { router } from './router';
import store from '@/core/store';
import Api from '@/core/service/api';
import { VERIFY } from '@/core/store/user/session.module';
import { RESET_LAYOUT_CONFIG, OVERRIDE_PAGE_LAYOUT_CONFIG } from '@/core/store/template/config.module';
import { SET_PARENT } from '@/core/store/template/breadcrumbs.module';

Vue.config.productionTip = false;

// Global 3rd party plugins
import 'popper.js';
import PerfectScrollbar from 'perfect-scrollbar';
window.PerfectScrollbar = PerfectScrollbar;
import ClipboardJS from 'clipboard';
window.ClipboardJS = ClipboardJS;

// Vue 3rd party plugins
import vuetify from '@/core/plugin/vuetify';
import '@/core/plugin/portal-vue';
import '@/core/plugin/bootstrap-vue';
import '@/core/plugin/perfect-scrollbar';
import '@/core/plugin/inline-svg';
import '@/core/plugin/apexcharts';
import '@/core/plugin/metronic';
import '@mdi/font/css/materialdesignicons.css';
import '@fortawesome/fontawesome-free/css/all.css';

// API service init
Api.init();

// Sync Router with Store
sync(store, router);

router.beforeEach((to, from, next) => {
  // Sync page layout with Sate
  const stateSync = ([
    {
      payload: { rank }
    }
  ]) => {
    if (rank.name === 'Employee') {
      store.dispatch(OVERRIDE_PAGE_LAYOUT_CONFIG, { aside: { items: { admin: false } } });
    }
    next();
  };

  // Ensure we checked auth before each page load
  document.title = `${to.meta.section || 'Phiscal'} | ${to.meta.title}`;

  // set parent page
  store.commit(SET_PARENT, to.meta.parent || false);

  // reset config to initial state
  store.dispatch(RESET_LAYOUT_CONFIG);

  switch (to.name) {
    case 'signedup':
    case 'redirect':
      next();
      break;
    default:
      Promise.all([store.dispatch(`session/${VERIFY}`)]).then(stateSync);
      break;
  }

  // Scroll page to top on every route change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App)
}).$mount('#app');
