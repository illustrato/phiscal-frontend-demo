'use strict';

import Api from '@/core/service/api';
import Http from '@/assets/js/logic/http';

// action types
export const MAKE_INQUIRY = 'makeInquiry';
export const NATIONS = 'getNations';
export const TNC = 'tnc';

const actions = {
  [MAKE_INQUIRY]({}, input) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.post(`/index/inquiry`, input).then(helper.followup).catch(helper.error);
    });
  },
  [NATIONS]({}, target = 'object') {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        let list;
        switch (target) {
          case 'object':
            list = {};
            for (const item of payload) {
              list[item.id] = item.name;
            }
            break;
          case 'array':
            list = [];
            for (const item of payload) {
              list.push({ text: item.name, value: item.id });
            }
            break;
        }
        resolve(list);
      };
      const helper = new Http(fx, reject);
      Api.rmHeaders();
      Api.get(`/index/nations`).then(helper.followup).catch(helper.error);
    });
  },
  [TNC]() {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.get(`/index/${TNC}`).then(helper.followup).catch(helper.error);
    });
  }
};

export default {
  namespaced: true,
  actions
};
