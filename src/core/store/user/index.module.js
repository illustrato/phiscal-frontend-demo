'use strict';

import Http from '@/assets/js/logic/http';
import Api from '@/core/service/api';
import { cacheItem, getItem, getCache, rmCache } from '@/core/service/storage';

// action types
export const CHECK = 'check';
export const DATA_USER = 'getUserData';
export const PASSWORD = 'passwdreset';
export const CRUD_AVATAR = 'crudAvatar';
export const UPDATE_USER = 'updateUser';
export const CRUD_FREELANCE = 'crudFreelance';
export const TITLES = 'titles';
export const TENANTS = 'lsUserTenants';

// mutation types
export const SET_ERROR = 'setError';
export const SET_FNAME = 'setFirstName';
export const SET_LNAME = 'setLastName';
export const SET_IDNO = 'setIdNumber';
export const SET_NATION = 'setNationality';
export const SET_TITLE = 'setTitle';
export const SET_DOB = 'setDateOfBirth';
export const SET_ADDR1 = 'setAddressLine1';
export const SET_ADDR2 = 'setAddressLine2';
export const SET_CITY = 'setCity';
export const SET_REGION = 'setRegion';
export const SET_COUNTRY = 'setCountry';
export const SET_GPS = 'setGPS';
export const SET_UNAME = 'setUsername';
export const SET_ALIAS = 'setAlias';
export const SET_EMAIL = 'setEmail';
export const SET_PHONE = 'setPhone';
export const SET_OLDPASSWD = 'setOldPassword';
export const SET_NEWPASSWD = 'setNewPassword';
export const SET_PASSWD_RESET = 'setPasswdReset';
export const SET_DATA = 'setDetails';
export const SET_GATEWAY = 'setGateway';
export const SET_CLIENT = 'setClient';
export const SET_KEY = 'setKey';

const state = {
  data: {},
  errors: [],
  passwd: {
    oldPassword: null,
    newPassword: null
  }
};

const getters = {
  /**
   * Determine if user requested changing Email Address
   * @param state
   * @returns {bool}
   */
  emailUpdate: function (state) {
    return !!state.data.email;
  }
};

const actions = {
  [CRUD_AVATAR]({}, blob) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.post(`/user/avatar`, blob).then(helper.followup).catch(helper.error);
    });
  },
  [CHECK]({}, { target, text }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/user/${CHECK}/${target}/${text}`).then(helper.followup).catch(helper.error);
    });
  },
  [CRUD_FREELANCE]({ state, commit }) {
    return new Promise((resolve, reject) => {
      if (Object.values(state.data).length === 0) {
        resolve({ payload: false });
        return;
      }
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.post(`/user/freelance`, { ...state.data, module: getCache('module') })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [DATA_USER]({}, target = 'default') {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.get(`/user/data/${target}`).then(helper.followup).catch(helper.error);
    });
  },
  [PASSWORD]({ state }) {
    return new Promise((resolve, reject) => {
      const fx = function (res) {
        rmCache('PSD');
        resolve(res);
      };
      const helper = new Http(fx, reject);
      Api.setBearer();
      Api.setHeader('PSD', getCache('PSD'));
      Api.put(`/user/${PASSWORD}`, {
        oldPassword: state.passwd.oldPassword,
        newPassword: state.passwd.newPassword
      })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [TENANTS]() {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.get(`/user/tenants`).then(helper.followup).catch(helper.error);
    });
  },
  [TITLES]() {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/user/${TITLES}`).then(helper.followup).catch(helper.error);
    });
  },
  async [UPDATE_USER]({ state, commit }) {
    return new Promise((resolve, reject) => {
      if (Object.values(state.data).length === 0) {
        resolve({ payload: false });
        return;
      }
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.post(`/user/update`, state.data).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  // text box
  [SET_ADDR1](state, payload) {
    payload = payload.trim();
    state.data.addressline1 = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_ADDR2](state, payload) {
    payload = payload.trim();
    state.data.addressline2 = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_ALIAS](state, payload) {
    payload = payload.trim();
    state.data.alias = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_CITY](state, payload) {
    payload = payload.trim();
    state.data.city = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_CLIENT](state, payload) {
    payload = payload.trim();
    state.data.client_id = payload.length === 0 ? '?' : payload;
  },
  // select box
  [SET_COUNTRY](state, payload) {
    state.data.country = payload === null ? '?' : payload;
  },
  // system
  [SET_DATA](state, payload) {
    state.data = payload;
  },
  // date control
  [SET_DOB](state, payload) {
    state.data.dob = payload;
  },
  // text box
  [SET_EMAIL](state, payload) {
    payload = payload.trim();
    state.data.email = payload.length === 0 ? '?' : payload;
  },
  // system
  [SET_ERROR](state, payload) {
    state.errors = payload;
  },
  // text box
  [SET_FNAME](state, payload) {
    payload = payload.trim();
    state.data.firstname = payload.length === 0 ? '?' : payload;
  },
  // select box
  [SET_GATEWAY](state, payload) {
    state.data.provider_id = payload === null ? '?' : payload;
  },
  // text box
  [SET_GPS](state, payload) {
    payload = payload.trim();
    state.data.gps = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_IDNO](state, payload) {
    payload = payload.trim();
    state.data.idnumber = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_KEY](state, payload) {
    payload = payload.trim();
    state.data.access_key = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_LNAME](state, payload) {
    payload = payload.trim();
    state.data.lastname = payload.length === 0 ? '?' : payload;
  },
  // select box
  [SET_NATION](state, payload) {
    state.data.nationality = payload === null ? '?' : payload;
  },
  // password box
  [SET_NEWPASSWD](state, payload) {
    state.passwd.newPassword = payload;
  },
  // password box
  [SET_OLDPASSWD](state, payload) {
    state.passwd.oldPassword = payload;
  },
  // system
  [SET_PASSWD_RESET](state, payload) {
    state.passwd.resetRequest = payload;
  },
  // text box
  [SET_PHONE](state, payload) {
    payload = payload.trim();
    state.data.phone = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_REGION](state, payload) {
    payload = payload.trim();
    state.data.region = payload.length === 0 ? '?' : payload;
  },
  // select box
  [SET_TITLE](state, payload) {
    state.data.title = payload === null ? '?' : payload;
  },
  // text box
  [SET_UNAME](state, payload) {
    payload = payload.trim();
    state.data.username = payload.length === 0 ? '?' : payload;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
