'use strict';

import Http from '@/assets/js/logic/http';
import { bool } from '@/assets/js/logic/hack';
import Api from '@/core/service/api';
import { cacheItem, destroyItem, getCache, getItem, rmCache, saveItem } from '@/core/service/storage';
import { router, isNavigationFailure, NavigationFailureType } from '@/router';

// action types
export const AUTH = 'userAuthentication';
export const EMAIL_UPDATE = 'updateemail';
export const FORGOT_LOGIN = 'forgotLogin';
export const FREEPASS = 'freePass';
export const LOGOUT = 'deletetoken';
export const READ_FEEDS = 'readfeeds';
export const RESET = 'passwdreset';
export const RM_USER = 'rmUser';
export const SIGNUP = 'register';
export const TENANT = 'tenant';
export const UPDATE_USER = 'updateUser';
export const VERIFY = 'verify';
export const VERIFY_EMAIL = 'emailVeification';

// mutation types
export const ADD_TENANT = 'addTenant';
export const CACHE_TENANT = 'setTenantId';
export const PURGE_AUTH = 'logOut';
export const SET_AUTH = 'setUser';
export const SET_AVATAR = 'setAvatar';
export const SET_BUSY = 'setPageBusy';
export const SET_FEEDS = 'setNotifications';
export const SET_MESSAGE = 'setMessage';
export const SET_MODULES = 'setModules';
export const SET_PASSWD = 'setPasswdSatus';
export const SET_SESSION = 'setDetails';
export const SET_TENANT = 'setTenant';
export const SET_TENANTS = 'setTenants';
export const SET_UFNAME = 'setUserFName';
export const SET_ULNAME = 'setUserLName';
export const UPDATE_FEEDS = 'mvNotifications';

const state = {
  authenticated: !!getItem('JWT'),
  busy: false,
  feeds: { alert: [], event: [], log: [], unread: 0 },
  message: null,
  rank: {},
  tenant: false,
  user: { tenants: [] }
};

const getters = {
  /**
   * Determine if there's a user logged in
   * @param state
   * @returns {bool}
   */
  authenticated: function (state) {
    return state.authenticated;
  },

  /**
   * Rank of Current User to current Organization
   * @param state
   * @returns {string}
   */
  currentRank: function (state) {
    return state.rank;
  },

  /**
   * Tenant of current session
   * @param state
   * @returns {object}
   */
  currentTenant: function (state) {
    return state.tenant;
  },

  /**
   * User Currently logged in
   * @param state
   * @returns {object}
   */
  currentUser: function (state) {
    return state.user;
  },

  /**
   * URL to user Avatar, or path to placeholder if user has no Avatar
   * @param state
   * @returns {string}
   */
  getAvatar: function (state) {
    let pic = state.user.avatar || false;
    return pic === false ? 'img/user/blank.png' : `${process.env.VUE_APP_FILES}/img/avatar/${state.user.id}.${state.user.avatar}`;
  },

  /**
   * Return Session Message to User
   * @param state
   * @returns {string}
   */
  getMessage: function (state) {
    return state.message || { visible: false, title: null, message: null };
  },

  /**
   * List of User Notifications
   * @param state
   * @returns {array}
   */
  notifications: function (state) {
    return state.feeds;
  },

  /**
   * Determine if there are pending Processes
   * @param state
   * @returns {bool}
   */
  pageBusy: function (state) {
    return state.busy;
  },

  /**
   * Determine if user requested reseting Password
   * @param state
   * @returns {bool}
   */
  resetRequested: function (state) {
    return !!getCache('PSD');
  }
};

const actions = {
  [AUTH]({ commit }, form) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(SET_AUTH, payload);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.post(`/session/auth`, form).then(helper.followup).catch(helper.error);
    });
  },
  [EMAIL_UPDATE]({ commit }, { uId, token }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.get(`/session/updateemail/${uId}/${token}`).then(helper.followup).catch(helper.error);
    });
  },
  [FORGOT_LOGIN]({ commit, state: { user } }, { email }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.post(`/session/forgotpasswd`, { email: user.email || email })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [FREEPASS]({ commit }, { uId, token }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        cacheItem('PSD', token);
        commit(SET_AUTH, payload);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.get(`/session/signin/${uId}/${token}`).then(helper.followup).catch(helper.error);
    });
  },
  [LOGOUT]({ commit }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(PURGE_AUTH);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.setBearer();
      Api.delete(`/session/${LOGOUT}`).then(helper.followup).catch(helper.error);
    });
  },
  [READ_FEEDS]({ commit }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(UPDATE_FEEDS, payload);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.setBearer();
      Api.get(`/session/${READ_FEEDS}`).then(helper.followup).catch(helper.error);
    });
  },
  [RM_USER]({}, email) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.delete(`/user/${email}`).then(helper.followup).catch(helper.error);
    });
  },
  [SIGNUP]({}, form) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      form.firstname = form.firstname.trim();
      form.lastname = form.lastname.trim();
      Api.post(`/session/register`, form).then(helper.followup).catch(helper.error);
    });
  },
  [VERIFY]({ commit }) {
    if (getItem('JWT')) {
      const promise = new Promise((resolve, reject) => {
        const helper = new Http(resolve, reject);
        Api.setBearer();
        Api.setHeader('Tenant', getCache('tenant'));
        Api.get(`/session/verify`).then(helper.followup).catch(helper.error);
      });
      promise
        .then(({ payload }) => {
          commit(SET_SESSION, payload);
          commit(SET_MODULES, payload);
          commit(SET_FEEDS, payload);
          return payload;
        })
        .catch(({ errors }) => {
          commit(PURGE_AUTH);
          router.push({ name: 'login', params: { errors: errors } }).catch((err) => {
            if (isNavigationFailure(err, NavigationFailureType.duplicated)) {
              for (const e of errors) {
                window.alert(e);
              }
              window.alert(`${err.from.path} to ${err.to.path}`);
            }
          });
        });
      return promise;
    } else {
      commit(PURGE_AUTH);
      return { payload: { tenant: false, rank: { name: 'visitor' } } };
    }
  },
  [VERIFY_EMAIL]({ commit }, { target, uId, token }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ headers, payload }) {
        if (typeof headers.module === 'string' && headers.module.length > 0) {
          cacheItem('module', headers.module);
        }
        commit(SET_AUTH, payload);
        resolve(payload);
      };
      const helper = new Http(target === 'revoke' ? resolve : fx, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/session/email/${target}/${uId}/${token}`).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  [ADD_TENANT](state, payload) {
    state.user.tenants.push(payload);
  },
  [CACHE_TENANT](state, payload) {
    cacheItem('tenant', payload);
  },
  [PURGE_AUTH](state) {
    destroyItem('JWT');
    rmCache('tenant');
    state.user = { tenants: '[]' };
    state.tenant = false;
    state.authenticated = !!getItem('JWT');
  },
  [SET_AUTH](state, payload) {
    saveItem('JWT', payload);
    state.authenticated = !!getItem('JWT');
  },
  [SET_MESSAGE](state, payload) {
    state.message = payload;
  },
  [SET_BUSY](state, payload) {
    state.busy = payload;
  },
  [SET_MODULES](state, { modules }) {
    if (state.tenant === false) {
      return;
    }
    let list = {};
    for (const item of modules) {
      item.active = bool(item.active);
      list[item.mod_id] = item;
      delete list[item.mod_id].mod_id;
    }
    state.tenant.modules = list;
  },
  [SET_AVATAR](state, payload) {
    state.user.avatar = payload;
  },
  [SET_FEEDS](state, { feeds }) {
    let notifications = { alert: [], event: [], log: [], unread: 0 };
    notifications.unread = feeds.unread;
    for (let i of feeds.records) {
      notifications[i.type].push(i);
    }
    state.feeds = notifications;
  },
  [SET_PASSWD](state, payload) {
    state.user.password = payload;
  },
  [SET_SESSION](state, { user, tenant, rank }) {
    if (tenant === false && user.tenants.length === 1) {
      cacheItem('tenant', user.tenants[0]);
    }
    state.user = user;
    state.rank = rank;
    state.tenant = tenant;
  },
  [SET_TENANT](state, { name, website }) {
    state.tenant.trademark = name;
    state.tenant.website = website;
  },
  [SET_TENANTS](state, payload) {
    state.user.tenants = payload;
  },
  [SET_UFNAME](state, payload) {
    state.user.firstname = payload;
    state.user.fullname = `${payload} ${state.user.lastname}`;
  },
  [SET_ULNAME](state, payload) {
    state.user.lastname = payload;
    state.user.fullname = `${state.user.firstname} ${payload}`;
  },
  [UPDATE_FEEDS](state, payload) {
    let notifications = { alert: [], event: [], log: [], unread: 0 };
    for (let i of payload) {
      notifications[i.type].push(i);
    }
    state.feeds = notifications;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
