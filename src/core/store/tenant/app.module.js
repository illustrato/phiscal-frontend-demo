'use strict';

import Http from '@/assets/js/logic/http';
import { bool } from '@/assets/js/logic/hack';
import Api from '@/core/service/api';
import { getItem, getCache } from '@/core/service/storage';

// action types
export const DATA_MODULE = 'getModuleData';
export const ELABORATE = 'elaborate';
export const INDEX = 'index';
export const SUBSCRIBE = 'subscribe';
export const LS_MODULE = 'lsModule';

// mutation types
export const SET_ID = 'setModId';
export const SET_NAME = 'setName';
export const SET_LOGO = 'setLogo';
export const SET_DESC = 'setDesc';
export const SET_ACTIVE = 'setActive';
export const SET_DATA = 'setData';
export const SET_ACTIVE_LIST = 'setActiveList';
export const SET_IDLING_LIST = 'setIdlingList';

const state = { data: {}, list: { active: [], idling: [] } };

const getters = {
  getLogo: function ({ data }) {
    let img = `${process.env.VUE_APP_FILES}/img/brand/phiscal/main.png`;
    return data.logo == null ? img : `${process.env.VUE_APP_FILES}/img/brand/module/${data.name}.${data.logo}`;
  },
  selectedModule: function ({ data }) {
    return data;
  },
  getModules: function ({ list }) {
    return list;
  }
};

const mutations = {
  [SET_ID](state, value) {
    state.data.mod_id = value;
  },
  [SET_NAME](state, value) {
    state.data.name = value;
  },
  [SET_LOGO](state, value) {
    state.data.logo = value;
  },
  [SET_DESC](state, value) {
    state.data.description = value;
  },
  [SET_ACTIVE](state, value) {
    state.data.active = bool(value);
  },
  [SET_DATA](state, payload) {
    state.data = payload;
    state.data.active = bool(payload.active);
  },
  [SET_ACTIVE_LIST](state, value) {
    state.list.active = value;
  },
  [SET_IDLING_LIST](state, value) {
    state.list.idling = value;
  }
};

const actions = {
  [DATA_MODULE]({ commit }, id) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(SET_DATA, payload);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/module/data/${id}`).then(helper.followup).catch(helper.error);
    });
  },
  [ELABORATE]({}, name) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/module/${ELABORATE}/${name}`).then(helper.followup).catch(helper.error);
    });
  },
  [INDEX]() {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/module/${INDEX}`).then(helper.followup).catch(helper.error);
    });
  },
  [LS_MODULE]({ commit }, target) {
    return new Promise((resolve, reject) => {
      const fx = (res) => {
        switch (target) {
          case 'active':
            commit(SET_ACTIVE_LIST, res.payload);
            break;
          case 'idling':
            commit(SET_IDLING_LIST, res.payload);
            break;
        }
        resolve(res);
      };
      const helper = new Http(fx, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.get(`/module/ls/${target}`).then(helper.followup).catch(helper.error);
    });
  },
  [SUBSCRIBE]({}, payload) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.post(`/module/${SUBSCRIBE}`, payload).then(helper.followup).catch(helper.error);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
