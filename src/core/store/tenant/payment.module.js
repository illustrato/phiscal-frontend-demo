'use strict';

import Api from '@/core/service/api';
import { getCache } from '@/core/service/storage';
import Http from '@/assets/js/logic/http';

// action types
export const ADD_GATEWAY = 'addgateway';
export const PAYMENT_DETAILS = 'details';
export const LS = 'ls';

// mutation types
export const SET_GATEWAY = 'setGateway';
export const SET_CLIENT = 'setClient';
export const SET_KEY = 'setKey';

const state = { data: {} };

const actions = {
  [ADD_GATEWAY]({ state }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      if (Object.values(state.data).length === 0) {
        resolve({ payload: false });
        return;
      }
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.post(`/payment/${ADD_GATEWAY}`, state.data).then(helper.followup).catch(helper.error);
    });
  },
  [LS]({}, { target, format }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        resolve(format(payload));
      };
      const helper = new Http(fx, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/payment/${LS}/${target}`).then(helper.followup).catch(helper.error);
    });
  },
  [PAYMENT_DETAILS]({}, target = 'self') {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.get(`/payment/details/${target}`).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  // select box
  [SET_GATEWAY](state, payload) {
    state.data.provider_id = payload === null ? '?' : payload;
  },
  // text box
  [SET_CLIENT](state, payload) {
    payload = payload.trim();
    state.data.client_id = payload.length === 0 ? '?' : payload;
  },
  // text box
  [SET_KEY](state, payload) {
    payload = payload.trim();
    state.data.access_key = payload.length === 0 ? '?' : payload;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
