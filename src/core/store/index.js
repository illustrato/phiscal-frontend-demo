import Vue from 'vue';
import Vuex from 'vuex';

import generic from './generic.module';

import config from './template/config.module';
import htmlclass from './template/htmlclass.module';
import breadcrumbs from './template/breadcrumbs.module';

import user from './user/index.module';
import session from './user/session.module';

import org from './org/index.module';
import branch from './org/branch.module';
import logo from './org/logo.module';
import staff from './org/staff.module';

import payment from './tenant/payment.module';
import app from './tenant/app.module';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: { generic, breadcrumbs, config, htmlclass, session, user, staff, branch, org, logo, payment, app }
});
