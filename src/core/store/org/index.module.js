'use strict';

import Api from '@/core/service/api';
import Http from '@/assets/js/logic/http';
import { cacheItem, destroyItem, getCache, getItem, rmCache, saveItem } from '@/core/service/storage';
import router from '@/router';

// action types
export const CRUD_ORG = 'crudOrg';
export const DATA_ORG = 'getOrgData';
export const RM_ORG = 'rmOrg';
export const TRADEMARK = 'trademark';
export const LS_ORG = 'lsOrg';

// mutation types
export const SET_DATA = 'setDepth';
export const SET_DBA = 'setDba';
export const SET_DESC = 'setDesc';
export const SET_NAME = 'setName';
export const SET_REGNO = 'setRegNo';
export const SET_REGISTRA = 'setRegistra';
export const SET_STRUCTURE = 'setStruct';
export const SET_WEB = 'setWeb';
export const SET_MOTTO = 'setMotto';
export const PURGE_ORG = 'purgeOrg';
export const SET_STAFF = 'setStaff';
export const SET_BRANCHES = 'setBranches';
export const SET_LOGOS = 'setLogos';
export const SET_MODULE = 'setModule';

const state = {
  data: {},
  logos: [],
  branches: [],
  staff: []
};

const getters = {
  /**
   * Return Organization Branches
   * @param state
   * @returns {array}
   */
  loadingOrg: function (state) {
    return state.busy;
  },

  /**
   * Return Organization Branches
   * @param state
   * @returns {array}
   */
  getBranches: function (state) {
    return state.branches;
  },

  /**
   * Return Headquarters
   * @param state
   * @returns {*}
   */
  getHQ: function (state) {
    for (let base of state.branches) {
      if (base.main) {
        return base;
      }
    }
    return false;
  },

  /**
   * Return Braing Logos
   * @param state
   * @returns {array}
   */
  getLogos: function (state) {
    return state.logos;
  },

  /**
   * Return Staff Members
   * @param state
   * @returns {array}
   */
  getStaff: function (state) {
    return state.staff;
  }
};

const actions = {
  [CRUD_ORG]({ state, commit }, logo = { picture: { base64: null } }) {
    return new Promise((resolve, reject) => {
      if (Object.values(state.data).length === 0) {
        resolve({ payload: false });
        return;
      }
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.post(`/org/crud`, { ...state.data, module: getCache('module'), ...logo })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [DATA_ORG]({ commit, rootState: { session } }) {
    return new Promise((resolve, reject) => {
      if (!session.tenant.org_id) {
        resolve({ payload: false });
        return;
      }
      const fx = ({ payload }) => {
        const branches = Object.values(payload.branches);
        commit(SET_BRANCHES, branches[0].branch < 1 ? [] : branches);
        const staff = Object.values(payload.staff);
        commit(SET_STAFF, staff[0].user_id === null ? [] : staff);
        let logos = Object.values(payload.logos);
        commit(SET_LOGOS, logos[0].ext === null ? [] : logos);

        resolve(payload);
      };
      const helper = new Http(fx, reject);
      const access = session.rank.name === 'Administrator' ? 'private' : 'public';

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.get(`/org/${access}data`).then(helper.followup).catch(helper.error);
    });
  },
  [LS_ORG]({}, { target, format }) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        resolve(format(payload));
      };
      const helper = new Http(fx, reject);
      Api.rmHeaders(['Authorization', 'Tenant']);
      Api.get(`/org/ls/${target}`).then(helper.followup).catch(helper.error);
    });
  },
  [RM_ORG]({ commit }, confirmation) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(PURGE_ORG);
        resolve(payload);
      };
      const helper = new Http(fx, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.delete(`/org/this/${confirmation}`).then(helper.followup).catch(helper.error);
    });
  },
  [TRADEMARK]({}, { id, affirm }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/org/switch/logo/${id}`, { positive: affirm }).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  [PURGE_ORG](state) {
    rmCache('tenant');
    state.logos = [];
    state.branches = [];
    state.staff = [];
  },
  [SET_BRANCHES](state, payload) {
    state.branches = payload;
  },
  [SET_DATA](state, payload) {
    state.data = payload;
  },
  [SET_DBA](state, payload) {
    state.data.dba = payload;
  },
  [SET_DESC](state, payload) {
    payload = payload.trim();
    state.data.description = payload.length === 0 ? '?' : payload;
  },
  [SET_LOGOS](state, payload) {
    state.logos = payload;
  },
  [SET_STAFF](state, payload) {
    state.staff = payload;
  },
  [SET_NAME](state, payload) {
    state.data.name = payload;
  },
  [SET_STRUCTURE](state, payload) {
    state.data.struct_id = payload;
  },
  [SET_REGISTRA](state, payload) {
    state.data.registra = payload;
  },
  [SET_REGNO](state, payload) {
    state.data.regno = payload;
  },
  [SET_WEB](state, payload) {
    state.data.website = payload;
  },
  [SET_MOTTO](state, payload) {
    state.data.motto = payload;
  },
  [SET_MODULE](state, payload) {
    state.data.module = payload;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
