'use strict';

import Api from '@/core/service/api';
import Http from '@/assets/js/logic/http';
import { getCache } from '@/core/service/storage';

// action types
export const CRUD_LOGO = 'crudLogo';

// mutation types
export const SELECT_LOGO = 'selectLogo';

const state = {
  busy: null,
  data: {},
  selected: null
};

const getters = {
  /**
   * Get currently select Logo
   * @param state
   * @returns {*}
   */
  selectedLogo: function (state) {
    return state.selected || 0;
  }
};

const actions = {
  [CRUD_LOGO]({}, { id, payload }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/logo/crud/${id}`, payload).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  [SELECT_LOGO](state, payload) {
    state.selected = payload;
  }
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};
