'use strict';

import Api from '@/core/service/api';
import Http from '@/assets/js/logic/http';
import { getCache } from '@/core/service/storage';

// action types
export const CRUD_EMP = 'crudEmp';
export const UPDATE = 'update';
export const CRUD_PHOTO = 'crudPhoto';
export const DELETE_EMP = 'rmEmp';

// mutation types
export const SELECT_EMP = 'selectEmp';
export const SET_BRANCH = 'setBranch';
export const SET_DATA = 'setData';
export const SET_DOB = 'setDOB';
export const SET_EMAIL = 'setEmail';
export const SET_FNAME = 'setFName';
export const SET_LNAME = 'setLName';
export const SET_NATION = 'setNation';
export const SET_IDNO = 'setIdNumber';
export const SET_TEL = 'setTelephone';
export const SET_WORK_EMAIL = 'setWorkEmail';

const state = {
  data: {},
  selected: null
};

const getters = {
  /**
   * Get currently select Employee
   * @param state
   * @returns {*}
   */
  selectedEmp: function (state) {
    return state.selected || false;
  }
};

const actions = {
  [CRUD_PHOTO]({ state: { selected } }, blob) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/staff/photo/${selected}`, blob).then(helper.followup).catch(helper.error);
    });
  },
  [CRUD_EMP]({ state: { data, selected } }, avatar = {}) {
    return new Promise((resolve, reject) => {
      if (Object.values(data).length === 0) {
        resolve({ payload: false });
        return;
      }
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/staff/crud/${selected || 'new'}`, { ...data, ...avatar })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [DELETE_EMP]({}, id) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', geCache('tenant'));
      Api.delete(`/staff/${id}`).then(helper.result).catch(helper.exception);
    });
  }
};

const mutations = {
  [SELECT_EMP](state, payload) {
    state.selected = payload;
  },
  [SET_BRANCH](state, payload) {
    state.data.branch = payload;
  },
  [SET_DATA](state, payload) {
    state.data = payload;
  },
  [SET_DOB](state, payload) {
    state.data.dob = payload;
  },
  [SET_EMAIL](state, payload) {
    state.data.email = payload;
  },
  [SET_FNAME](state, payload) {
    payload = payload.trim();
    state.data.firstname = payload;
  },
  [SET_IDNO](state, payload) {
    state.data.idnumber = payload;
  },
  [SET_LNAME](state, payload) {
    payload = payload.trim();
    state.data.lastname = payload;
  },
  [SET_TEL](state, payload) {
    state.data.telephone = payload;
  },
  [SET_NATION](state, payload) {
    state.data.nationality = payload;
  },
  [SET_WORK_EMAIL](state, payload) {
    state.data.workEmail = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
