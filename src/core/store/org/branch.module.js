'use strict';

import Api from '@/core/service/api';
import Http from '@/assets/js/logic/http';
import { getCache } from '@/core/service/storage';

// action types
export const CRUD_BRANCH = 'crudBranch';
export const BRANCH_PHOTO = 'crudPhoto';
export const DELETE_BRANCH = 'rmBranch';
export const HEADQUARTERS = 'headquarters';

// mutation types
export const SELECT_BRANCH = 'selectBranch';
export const SET_DATA = 'setData';
export const SET_ADDR1 = 'setAddressLine1';
export const SET_ADDR2 = 'setAddressLine2';
export const SET_CITY = 'setCity';
export const SET_REGION = 'setRegion';
export const SET_COUNTRY = 'setCountry';
export const SET_GPS = 'setGPS';
export const SET_DESC = 'setDesc';
export const SET_PHONE = 'setPhone';
export const SET_HQ = 'setHeadquarters';

const state = {
  data: {},
  selected: null
};

const getters = {
  /**
   * Get currently select Logo
   * @param state
   * @returns {*}
   */
  selectedBranch: function (state) {
    return state.selected || 0;
  }
};

const actions = {
  [BRANCH_PHOTO]({ state: { selected } }, payload) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/branch/photo/${selected}`, payload).then(helper.followup).catch(helper.error);
    });
  },
  [CRUD_BRANCH]({ state: { selected, data } }, photo = {}) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      if (Object.values(data).length === 0) {
        resolve({ payload: false });
        return;
      }

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/branch/crud/${selected || 0}`, { ...data, ...photo })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [DELETE_BRANCH]({}, id) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.delete(`/branch/${id}`).then(helper.followup).catch(helper.error);
    });
  },
  [HEADQUARTERS]({}, { hq, positive }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);

      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.put(`/org/switch/branch/${hq}`, { positive: positive }).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  [SELECT_BRANCH](state, payload) {
    state.selected = payload;
  },
  [SET_ADDR1](state, payload) {
    state.data.addressline1 = payload || '?';
  },
  [SET_ADDR2](state, payload) {
    state.data.addressline2 = payload || '?';
  },
  [SET_CITY](state, payload) {
    state.data.city = payload || '?';
  },
  [SET_REGION](state, payload) {
    state.data.region = payload || '?';
  },
  [SET_COUNTRY](state, payload) {
    state.data.country = payload || '?';
  },
  [SET_GPS](state, payload) {
    state.data.gps = payload || '?';
  },
  [SET_DESC](state, payload) {
    state.data.description = payload || '?';
  },
  [SET_PHONE](state, payload) {
    payload = typeof payload === 'string' ? payload.trim() : '';
    state.data.telephone = payload.length === 0 ? '?' : payload;
  },
  [SET_HQ](state, payload) {
    state.data.main = payload;
  },
  [SET_DATA](state, payload) {
    state.data = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
